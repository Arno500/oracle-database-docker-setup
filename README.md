# Oracle DB Docker container
> Based on the Dockerfiles at https://github.com/oracle/docker-images/tree/main/OracleDatabase/SingleInstance/dockerfiles/21.3.0

### Oracle DB version: 21.3.0

## Preparation
What you need:
- Windows 10 or 11 (the latest the better),
- Docker (or Podman if you know how to use it, whatever, any container system supporting Docker-type containers will do). Instructions here: https://www.docker.com/products/docker-desktop.
In any case, please be sure to have the latest version installed (and update it if needed).

> **Warning**: Installing Docker on Windows will enable the *Windows Container Platform* component containing the *Hyper-V* engine. It's not an issue by itself, but some programs may consider that as a threat. For instance, *FACEIT Anti-Cheat* will not start with it enabled (`"bECauSe yOU cAn CHeAt wITh hYPeR-V"`). You can always disable it in the "Add or remove features" (but bear in mind Docker will not start without this feature reenabled).

Please be sure you have the rights to execute the scripts inside the directory for your platform. On Linux and macOS, you may need to `chmod +x script.sh` to each file to "unlock" them. 

## Build

This is only needed once (unless the Dockerfile have been changed).  
Run `buildImage.bat` (Windows) or `buildImage.sh`.

## Create the container

This is only needed each time you want to start fresh (after erasing the old container).  
Run `runImage.bat` (Windows) or `runImage.sh`.

## ShellPlus (Oracle's SQL CLI)

Run `shellPlus.bat` (Windows) or `shellPlus.sh`.  
Note that this is the worst tool, the formatting is ugly, and you can only do queries.  
I personnally recommend you to use either:
- [**VSCode**](https://code.visualstudio.com/) with the [*Oracle Developer Tools for VS Code (SQL and PLSQL)*](https://marketplace.visualstudio.com/items?itemName=Oracle.oracledevtools) extension, using the following settings:
    - Database host name: `localhost`
    - Port number: `1521`
    - Service name: `XE`
    - User name: `SYSTEM`
    - Password: `root`
    - Save password: ✅ (optional)  

    After that, you can create `.sql` files, verify that they are open in the "Oracle-SQL and PLSQL" mode, then click on the "Disconnected" button and choose the connection you just added if it's not already connected. You should now be able to execute SQL by right clicking on something and "Execute SQL" or "Execute All" (or using the associated keybinds). ![VSCode visual instructions](./images/VSCode.png "VSCode visual instructions")
- [**Datagrip**](https://www.jetbrains.com/fr-fr/datagrip/): I don't have the specifics of the connection, but you may use the same informations as before, mainly the username is `SYSTEM` and all the passwords are set as `root`.
- [**Oracle SQL Developer**](https://www.oracle.com/tools/downloads/sqldev-downloads.html): It's a shitty and ugly Java software that you don't want to use. However, it's the most complete one if you need to use the advanced features of the database (probably not for this class, though). Also, you need an Oracle account to download it 🤡.

## Multiple instances
To run multiple instances, you can run the script `runImage2.bat` (Windows) or `runImage2.sh`. It will start a complete new Oracle DB instance (so please wait a bit for it to initialize the new DB...).  
After that, you may be able to run the script `addSecondaryDB.sql` on the first DB in the directory `Common` to create the DB link to the second database.  
The settings to connect to the second DB are the same as the first, except the port is `1522` instead of `1521`. You may then execute queries using the `db2` DBLink!