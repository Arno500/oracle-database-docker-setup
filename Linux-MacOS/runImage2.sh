#!/bin/bash
docker run --name oracle-database --platform linux/amd64 -p 1522:1521 -e ORACLE_PWD=root -v /opt/oracle/oradata "oracle/database:latest-xe"