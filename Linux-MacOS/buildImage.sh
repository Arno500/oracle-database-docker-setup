#!/bin/bash
SCRIPT_PATH=`dirname "$0"`
docker build --platform linux/amd64 --build-arg DB_EDITION="xe" -t "oracle/database:latest-xe" -f "$SCRIPT_PATH/../Dockerfile.xe" "$SCRIPT_PATH/.."