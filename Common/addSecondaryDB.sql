create database link db2
connect to SYSTEM identified by root
using '(DESCRIPTION =
(ADDRESS_LIST =
  (ADDRESS = (PROTOCOL = TCP)(HOST = host.docker.internal)(PORT = 1522))
)
(CONNECT_DATA =
  (SERVICE_NAME = XE)
)
)';
select * from dba_db_links;